# PROJECT INFO

TASK MANAGER

# DEVELOPER INFO

**NAME**: ALINA BAKAEVA

**E-MAIL**: Bak_Al@bk.ru

# SOFTWARE 

- JDK 1.8
- MS WINDOWS 10

# PROJECT BUILD

```bash
mvn clean build
```

# PROGRAM RUN

```bash
java -jar. / task-manager-1.0.0.jar
```