package com.bakaeva.tm.command;

import com.bakaeva.tm.api.service.IServiceLocator;
import com.bakaeva.tm.enumerated.Role;

public abstract class AbstractCommand {

    protected IServiceLocator serviceLocator;

    public AbstractCommand() {
    }

    public void setServiceLocator(IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public Role[] roles() {
        return null;
    }

    public abstract String name();

    public abstract String argument();

    public abstract String description();

    public abstract void execute();

}