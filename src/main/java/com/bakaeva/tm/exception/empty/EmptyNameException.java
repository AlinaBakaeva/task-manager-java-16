package com.bakaeva.tm.exception.empty;

public class EmptyNameException extends RuntimeException {

    public EmptyNameException() {
        super("Error! Name is empty...");
    }

}