package com.bakaeva.tm.exception.empty;

public class EmptyIdException extends RuntimeException {

    public EmptyIdException() {
        super("Error! ID  is empty...");
    }

}