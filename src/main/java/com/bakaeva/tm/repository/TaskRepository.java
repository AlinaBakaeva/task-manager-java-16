package com.bakaeva.tm.repository;

import com.bakaeva.tm.api.repository.ITaskRepository;
import com.bakaeva.tm.entity.Task;

import java.util.ArrayList;
import java.util.List;

public class TaskRepository implements ITaskRepository {
    private List<Task> tasks = new ArrayList<>();

    @Override
    public void add(final String userId, final Task task) {
        tasks.add(task);
        task.setUserId(userId);
    }

    @Override
    public void remove(final String userId, final Task task) {
        if (!userId.equals(task.getUserId())) return;
        tasks.remove(task);
    }

    @Override
    public List<Task> findAll(final String userId) {
        final List<Task> result = new ArrayList<>();
        for (final Task task : tasks) {
            if (userId.equals(task.getUserId())) result.add(task);
        }
        return result;
    }

    @Override
    public void clear(final String userId) {
        final List<Task> userTasks = findAll(userId);
        tasks.removeAll(userTasks);
    }

    @Override
    public Task findById(final String userId, final String id) {
        for (final Task task : tasks) {
            if (id.equals((task.getId())) && userId.equals(task.getUserId()))
                return task;
        }
        return null;
    }

    @Override
    public Task findByIndex(final String userId, final Integer index) {
        final List<Task> userTasks = new ArrayList<>();
        for (final Task task : tasks) {
            if (userId.equals(task.getUserId())) userTasks.add(task);
        }
        return userTasks.get(index);
    }

    @Override
    public Task findByName(final String userId, final String name) {
        for (final Task task : tasks) {
            if (name.equals((task.getName())) && userId.equals(task.getUserId()))
                return task;
        }
        return null;
    }

    @Override
    public Task removeById(final String userId, final String id) {
        Task task = findById(userId, id);
        if (task == null) return null;
        remove(userId, task);
        return task;
    }

    @Override
    public Task removeByIndex(final String userId, final Integer index) {
        final Task task = findByIndex(userId, index);
        if (task == null) return null;
        remove(userId, task);
        return task;
    }

    @Override
    public Task removeByName(final String userId, final String name) {
        final Task task = findByName(userId, name);
        if (task == null) return null;
        remove(userId, task);
        return task;
    }

}