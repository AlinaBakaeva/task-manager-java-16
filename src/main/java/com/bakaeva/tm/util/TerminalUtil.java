package com.bakaeva.tm.util;

import com.bakaeva.tm.exception.NotNumberException;

import java.util.Scanner;

public interface TerminalUtil {

    Scanner SCANNER = new Scanner(System.in);

    static String nextLine() {
        return SCANNER.nextLine();
    }

    static Integer nextNumber() {
        final String value = nextLine();
        try {
            return Integer.parseInt(value);
        } catch (Exception e){
            throw new NotNumberException(value);
        }

    }

}