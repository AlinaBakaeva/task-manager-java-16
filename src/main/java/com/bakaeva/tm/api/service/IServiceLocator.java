package com.bakaeva.tm.api.service;

public interface IServiceLocator {

    IUserService getUserService();

    IAuthService getAuthService();

    ITaskService getTaskService();

    IProjectService getProjectService();

}